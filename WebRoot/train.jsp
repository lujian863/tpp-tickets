<%@ page language="java" import="java.util.*,cn.njau.tpp.dbo.Train" pageEncoding="UTF-8"%>
<jsp:useBean id="cmb" scope="page" class="cn.njau.tpp.bean.CommonBean" />
<!-- 加载头部  -->
<%@ include file="inc/header.jsp"%>
<%
	Map<Integer, Integer> tk = cmb.getTickets();
	Map<Integer, String> cmap = cmb.getCities();
	List<Train> train = (List<Train>) request.getAttribute("tlist");
	Map<Integer, String> kind = new HashMap<Integer, String>();
	kind.put(0, "K(快车)");
	kind.put(1, "T(特快)");
	kind.put(2, "D(动车)");
	kind.put(3, "G(高铁)");
	kind.put(4, "L(临时)");
%>

<div class="container-fluid mt50">
	<div class="search-list">
		<h1 align="center">
			查询结果
		</h1>
		<table class="table table-hover">
			<thead>
				<tr>
					<th>
						车次
					</th>
					<th>
						出发城市
					</th>
					<th>
						出发时间
					</th>
					<th>
						到达城市
					</th>
					<th>
						到达时间
					</th>
					<th>
						价格
					</th>
					<th>
						余票
					</th>
					<th>
						预订
					</th>
				</tr>
			</thead>
			<%
				for (Train t : train) {
			%>
			<form method="post" action="book.jsp">
				<input type="hidden" name="tid" value="<%=t.getTid()%>">
				<tr>
					<td>
						<%=kind.get(t.getTkind())%><%=t.getTcode()%>
					</td>
					<td>
						<%=cmap.get(t.getTscid())%>
					</td>
					<td>
						<%=t.getTstime()%>
					</td>
					<td>
						<%=cmap.get(t.getTfcid())%>
					</td>
					<td>
						<%=t.getTftime()%>
					</td>
					<td>
						<%=t.getTprice()%>元
					</td>
					<td>
						<%=tk.get(t.getTid()) %>张
					</td>
					<td>
						<%
							if (tk.get(t.getTid()) > 0) {
						%>
						<input type="submit" class="btn btn-primary btn-small" value="预订" />
						<%
							} else {
						%>
						不能预订
						<%
							}
						%>
					</td>
				</tr>
			</form>
			<%
				}
			%>
		</table>
	</div>
	<!--/row-->

	<!-- 加载底部  -->
	<%@ include file="inc/footer.jsp"%>