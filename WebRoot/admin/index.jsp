<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!-- 加载头部  -->
<%@ include file="header.jsp" %>

<div class="container-fluid mt30">
	<div class="span2">
		&nbsp;
	</div>
	<div class="hero-unit span3">
		<p class="f30">
			用户管理
		</p>
		<p class="mt20 text-right">
			<a href="users.jsp" class="btn btn-primary btn-large">进入 >></a>
		</p>
	</div>

	<div class="hero-unit span3">
		<p class="f30">
			订单管理
		</p>
		<p class="mt20 text-right">
			<a href="orders.jsp" class="btn btn-primary btn-large">进入 >></a>
		</p>
	</div>
	<div class="span2">
		&nbsp;
	</div>
	<div class="hero-unit span3">
		<p class="f30">
			车次管理
		</p>
		<p class="mt20 text-right">
			<a href="trains.jsp" class="btn btn-primary btn-large">进入 >></a>
		</p>
	</div>

	<div class="hero-unit span3">
		<p class="f30">
			退出系统
		</p>
		<p class="mt20 text-right">
			<a href="../login.jsp" class="btn btn-danger btn-large">Logout >></a>
		</p>
	</div>
</div>
<!--/row-->

<!-- 加载底部  -->
<%@ include file="footer.jsp"%>