<%@ page language="java" import="java.util.*,cn.njau.tpp.dbo.User" pageEncoding="UTF-8"%>
<jsp:useBean id="adb" scope="page" class="cn.njau.tpp.bean.AdminBean" />
<!-- 加载头部  -->
<%@ include file="header.jsp"%>
<%
	List<User> users = adb.getUsers();
%>
<div class="container-fluid mt50">

	<div class="search-list">
		<table class="table table-hover">
			<tr>
				<td>用户名</td>
				<td>用户邮箱</td>
				<td>类型</td>
				<td>操作</td>
			</tr>
			<%
				for (User u : users) {
			%>
			<tr>
				<td>
					<%=u.getUname()%>
				</td>
				<td>
					<%=u.getUmail()%>
				</td>
				<td>
					<%=u.getUkindx()%>
				</td>
				<td>
					<a href="#" class="btn btn-primary btn-small">停用</a>
				</td>
			</tr>
			<%
				}
			%>
		</table>
	</div>

</div>
<!--/row-->

<!-- 加载底部  -->
<%@ include file="footer.jsp"%>