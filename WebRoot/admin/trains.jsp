<%@ page language="java" import="java.util.*,cn.njau.tpp.dbo.Train" pageEncoding="UTF-8"%>
<jsp:useBean id="cmb" scope="page" class="cn.njau.tpp.bean.CommonBean" />

<!-- 加载头部  -->
<%@ include file="header.jsp"%>
<%
	Map<Integer, Integer> tk = cmb.getTickets();
	Map<Integer, String> cmap = cmb.getCities();
	List<Train> train = cmb.getTrains();
	Map<Integer, String> kind = new HashMap<Integer, String>();
	kind.put(0, "K(快车)");
	kind.put(1, "T(特快)");
	kind.put(2, "D(动车)");
	kind.put(3, "G(高铁)");
	kind.put(4, "L(临时)");
%>
<div class="container-fluid mt50">
	<form class="form-search" method="post" action="train.add">
		<h2 class="form-search-heading" align="center">
			添加车次
		</h2>
		<div class="form-s-row">
			出发城市：
			<select name="tscid">
				<%
					for (int k : cmap.keySet()) {
				%>
				<option value="<%=k%>"><%=cmap.get(k)%></option>
				<%
					}
				%>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 出发时间：
			<div class="input-append date" id="picktime1">
				<input type="text" name="tstime" data-format="yyyy-MM-dd hh:mm" />
				<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i> </span>
			</div>
		</div>
		<div class="form-s-row">
			到达城市：
			<select name="tfcid">
				<%
					for (int k : cmap.keySet()) {
				%>
				<option value="<%=k%>"><%=cmap.get(k)%></option>
				<%
					}
				%>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 到达时间：
			<div class="input-append date" id="picktime2">
				<input type="text" name="tftime" data-format="yyyy-MM-dd hh:mm" />
				<span class="add-on"> <i data-time-icon="icon-time" data-date-icon="icon-calendar"></i> </span>
			</div>
		</div>
		<div class="form-s-row">
			车次：
			<select name="tkind" class="w100">
				<option value="0">
					K(快车)
				</option>
				<option value="1">
					T(特快)
				</option>
				<option value="2">
					D(动车)
				</option>
				<option value="3">
					G(高铁)
				</option>
				<option value="4">
					L(临时)
				</option>
			</select>
			<input type="text" name="tcode" class="w135" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 票数：
			<input type="text" name="tnum" class="w100" />
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 价格：
			<input type="text" name="tprice" class="w100" />
		</div>

		<div class="form-s-btn">
			<button class="btn btn-primary" type="submit">
				添 加
			</button>
		</div>
	</form>

	<div class="search-list">
		<table class="table table-hover">
			<tr>
				<td>
					车次
				</td>
				<td>
					出发城市
				</td>
				<td>
					出发时间
				</td>
				<td>
					到达城市
				</td>
				<td>
					到达时间
				</td>
				<td>
					价格
				</td>
				<td>
					剩余票数
				</td>
				<td>
					操作
				</td>
			</tr>
			<%
				if (train != null) {
					for (Train t : train) {
			%>
			<tr>
				<td>
					<%=kind.get(t.getTkind())%><%=t.getTcode()%>
				</td>
				<td>
					<%=cmap.get(t.getTscid())%>
				</td>
				<td>
					<%=t.getTstime()%>
				</td>
				<td>
					<%=cmap.get(t.getTfcid())%>
				</td>
				<td>
					<%=t.getTftime()%>
				</td>
				<td>
					<%=t.getTprice()%>元
				</td>
				<td>
					<%=tk.get(t.getTid()) %>张
				</td>
				<td>
					<input type="button" class="btn btn-primary btn-small" value="删除" />
				</td>
			</tr>
			<%
				}
				} else {
			%>
			<tr>
				<td>
					无信息
				</td>
				<td>
					无信息
				</td>
				<td>
					无信息
				</td>
				<td>
					无信息
				</td>
				<td>
					无信息
				</td>
				<td>
					无信息
				</td>
				<td>
					无信息
				</td>
				<td>
					无信息
				</td>
			</tr>
			<%
				}
			%>
		</table>
	</div>

</div>
<!--/row-->

<!-- 加载底部  -->
<%@ include file="footer.jsp"%>
<script type="text/javascript">
	$(function() {
		$('#picktime1').datetimepicker({
			language : 'zh-CN'
		// language: 'pt-BR'
		});
		$('#picktime2').datetimepicker({
			language : 'zh-CN'
		// language: 'pt-BR'
		});
	});
</script>