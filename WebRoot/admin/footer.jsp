<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<hr />
<footer>
<p align="center">火车票预订系统 &copy; 南农科技 2013</p>
</footer>

</div><!--/.fluid-container-->



<!-- 关于 -->
<div id="aboutModal" class="modal hide fade">
	<div class="modal-header">
    	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    	<h3>关于</h3>
  	</div>
    <div class="modal-body">
   		<div class="control-group">
    		<div class="controls f16">火车票预订系统 &copy; 南农科技 2013</div>
  		</div>
        <div class="control-group">
    		<div class="controls">Version 1.0</div>
  		</div>
        <div class="control-group">
    		<div class="controls">2013-04-10</div>
  		</div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal">确定</button>
  	</div>
</div>


</body>
</html>