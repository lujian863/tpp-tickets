<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!-- 加载头部  -->
<%@ include file="inc/header.jsp" %>

<div class="container-fluid mt30">
	<div class="text-center">
	<div class="hero-unit span3">
		<p class="f30">
			我的资料
		</p>
		<p class="mt20 text-right">
			<a href="main.jsp" class="btn btn-primary btn-large">进入 >></a>
		</p>
	</div>

	<div class="hero-unit span3">
		<p class="f30">
			我的订单
		</p>
		<p class="mt20 text-right">
			<a href="order.jsp" class="btn btn-primary btn-large">进入>></a>
		</p>
	</div>
	</div>
	<div class="hero-unit span3">
		<p class="f30">
			车票查询
		</p>
		<p class="mt20 text-right">
			<a href="search.jsp" class="btn btn-primary btn-large">Go >></a>
		</p>
	</div>

	<div class="hero-unit span3">
		<p class="f30">
			退出系统
		</p>
		<p class="mt20 text-right">
			<a href="login.jsp" class="btn btn-danger btn-large">Logout >></a>
		</p>
	</div>
</div>
<!--/row-->

<!-- 加载底部  -->
<%@ include file="inc/footer.jsp"%>
