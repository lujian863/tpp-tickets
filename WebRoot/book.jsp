<%@ page language="java" import="java.util.*,cn.njau.tpp.dbo.*" pageEncoding="UTF-8"%>
<jsp:useBean id="cmb" scope="page" class="cn.njau.tpp.bean.CommonBean" />
<!-- 加载头部  -->
<%@ include file="inc/header.jsp"%>

<%
	List<Man> men = cmb.getMyMen((Integer) request.getSession().getAttribute("uid"));
	Map<Integer, Integer> tk = cmb.getTickets();
	Map<Integer, String> cmap = cmb.getCities();
	int tid = Integer.parseInt(request.getParameter("tid"));
	Train t = new Train(tid);
	Map<Integer, String> kind = new HashMap<Integer, String>();
	kind.put(0, "K(快车)");
	kind.put(1, "T(特快)");
	kind.put(2, "D(动车)");
	kind.put(3, "G(高铁)");
	kind.put(4, "L(临时)");
	
	Man tmp = null;
%>

<div class="container-fluid mt50">
	<div class="search-list">
		<h1 align="center">
			车票预订
		</h1>
		<table class="table table-hover">
			<tr>
				<td>
					车次
				</td>
				<td>
					出发城市
				</td>
				<td>
					出发时间
				</td>
				<td>
					到达城市
				</td>
				<td>
					到达时间
				</td>
				<td>
					价格
				</td>
				<td>
					余票
				</td>
				<td>
					操作
				</td>
			</tr>
			<tr>
				<td>
					<%=kind.get(t.getTkind()) %><%=t.getTcode() %>
				</td>
				<td>
					<%=cmap.get(t.getTscid())%>
				</td>
				<td>
					<%=t.getTstime()%>
				</td>
				<td>
					<%=cmap.get(t.getTfcid())%>
				</td>
				<td>
					<%=t.getTftime()%>
				</td>
				<td>
					<%=t.getTprice()%>元
				</td>
				<td>
					<%=tk.get(t.getTid()) %>张
				</td>
				<td>
					<a href="search.jsp" class="btn btn-danger btn-small">取消</a>
				</td>
			</tr>
		</table>
	</div>
	<form class="form-search" method="post" action="book.do">
		<input type="hidden" name="tid" value="<%=tid %>">
		<input type="hidden" name="num" value="1" id="num">
		<input type="hidden" name="test[1]" value="1">
		<input type="hidden" name="test[2]" value="2">
		<input type="hidden" name="test[4]" value="3">
		<p class="text-center mb30">
			<select id="hman">
				<%for(Man m : men){ 
					if(m.getIsdefault() == 0){
						tmp = m;
					}
				%>
				<option value="<%=m.getMname() %>|<%=m.getMcode() %>">
					<%=m.getMname() %>（<%=m.getMcode() %>）
				</option>
				<%} %>
			</select>
			<a href="javascript:void(0)" class="btn btn-primary btn-small" id="hadd">从历史名单中添加</a>
		</p>
		<ol id="men">
			<li class="text-center" id="man-1">
				姓名：
				<input type="text" name="tname-1" value="<%if(tmp != null){ out.println(tmp.getMname());} %>" />
				&nbsp;&nbsp;&nbsp; 身份证号：
				<input type="text" name="mcode-1" value="<%if(tmp != null){ out.println(tmp.getMcode());} %>" />
				<a href="javascript:void(0)" class="btn btn-mini btn-primary" id="add">添加</a>
			</li>
		</ol>
		<p class="text-center">
			<input type="submit" class="btn btn-primary" value="提交订单" />
		</p>
	</form>
</div>

</div>
<!--/row-->
<script>
	var num = 1;
	var tnum = 1;
	$(document).ready(function() {
		$('#hadd').click(function() {
			if(tnum >= 5){
				alert('最多同时预订5张票！');
				return;
			}
			num++;
			tnum++;
			var id = num;
			var hman = $('#hman option:selected').val();
			var name = hman.split('|')[0];
			var code = hman.split('|')[1];
			insertHMan(id, name, code);
			$('#del-' + id).click(function() {
				$('#man-' + id).remove();
				tnum--;
			});
		});

		$('#add').click(function() {
			if(tnum >= 5){
				alert('最多同时预订5张票！');
				return;
			}
			num++;
			tnum++;
			var id = num;
			insertMan(id);
			$('#del-' + id).click(function() {
				$('#man-' + id).remove();
				tnum--;
			});
		});
	});

	function insertMan(id) {
		var html = '<li class="text-center" id="man-'+ id +'"> ';
		html += '姓名： ';
		html += '<input type="text" name="tname-'+ id +'" /> ';
		html += '&nbsp;&nbsp;&nbsp; 身份证号： ';
		html += '<input type="text" name="mcode-'+ id +'" /> ';
		html += '<a href="javascript:void(0)" class="btn btn-mini btn-primary" id="del-'
				+ id + '">删除</a> ';
		html += '</li> ';
		$('#men').append(html);
	}

	function insertHMan(id, name, code) {
		var html = '<li class="text-center" id="man-'+ id +'"> ';
		html += '姓名： ';
		html += '<input type="text" name="tname-'+ id +'" value="'+ name +'" /> ';
		html += '&nbsp;&nbsp;&nbsp; 身份证号： ';
		html += '<input type="text" name="mcode-'+ id +'" value="'+ code +'" /> ';
		html += '<a href="javascript:void(0)" class="btn btn-mini btn-primary" id="del-'
				+ id + '">删除</a> ';
		html += '</li> ';
		$('#men').append(html);
	}

	function numPlus() {
		$('#num').attr('value', tnum);
	}
</script>
<!-- 加载底部  -->
<%@ include file="inc/footer.jsp"%>