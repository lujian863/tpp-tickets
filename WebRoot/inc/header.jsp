<%@ page language="java" import="java.util.*,cn.njau.tpp.conf.Conf" pageEncoding="UTF-8"%>
<%
	// 全局加载
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
	String title = Conf.WEB_TITLE;

	// 登录判断
	if (session.getAttribute("uid") == null) {
		response.sendRedirect("login.jsp");
		return;
	}
	String uname = session.getAttribute("uname").toString();
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title><%=title%></title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">
		<link rel="stylesheet" href="css/datepicker.css" />

		<link rel="stylesheet" href="css/base.css" media="screen" />
		<link rel="stylesheet" href="css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" media="screen" />
		<link rel="stylesheet" href="css/common.css" media="screen" />
		<script src="js/jquery-1.8.0.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/common.js"></script>
		<script src="js/bootstrap-datepicker.js"></script>
		<script src="js/locales/bootstrap-datepicker.zh-CN.js"></script>
	</head>

	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="#"><%=title %></a>
					<div class="nav-collapse collapse">
						<ul class="nav navbar-text pull-right">
							<li>
								<a href="#" class="navbar-link"><%=uname%></a>
							</li>
							<li>
								<a class="navbar-link" href="logout.jsp" data-toggle="modal">注销</a>
							</li>
						</ul>
						<ul class="nav">
							<li class="active">
								<a href="index.jsp">首页</a>
							</li>
							<li>
								<a href="#aboutModal" data-toggle="modal">关于</a>
							</li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>