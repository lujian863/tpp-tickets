<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<hr />
<footer>
<p align="center">
	火车票预订系统 &copy; 南农科技 2013
</p>
</footer>

</div>
<!--/.fluid-container-->

<!-- 关于 -->
<div id="aboutModal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h3>
			关于
		</h3>
	</div>
	<div class="modal-body">
		<div class="control-group">
			<div class="controls f16">
				火车票预订系统 &copy; 南农科技 2013
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				Version 1.0
			</div>
		</div>
		<div class="control-group">
			<div class="controls">
				2013-04-10
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<button class="btn" data-dismiss="modal">
			确定
		</button>
	</div>
</div>

<!-- 注册 -->
<div id="regModal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h3>
			注册
		</h3>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" method="post" action="register.do">
			<div class="control-group">
				<label class="control-label" for="inputUname">
					账号：
				</label>
				<div class="controls">
					<input type="text" name="uname" id="inputUname" placeholder="账号">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputEmail">
					邮箱：
				</label>
				<div class="controls">
					<input type="text" name="umail" id="inputEmail" placeholder="Email">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">
					密码：
				</label>
				<div class="controls">
					<input type="password" name="upass" id="inputPassword" placeholder="Password">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputPassword">
					再次输入密码：
				</label>
				<div class="controls">
					<input type="password" name="upass2" id="inputPassword" placeholder="Password">
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-primary">
						注册
					</button>
					<button class="btn" data-dismiss="modal">
						取消
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

</body>
</html>