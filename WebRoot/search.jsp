<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<jsp:useBean id="cmb" scope="page" class="cn.njau.tpp.bean.CommonBean" />
<!-- 加载头部  -->
<%@ include file="inc/header.jsp" %>
<%
	Map<Integer,String> cmap = cmb.getCities();
%>
<div class="container-fluid mt100">
	<form class="form-search" method="post" action="search.do">
		<h2 class="form-search-heading" align="center">
			火车票查询
		</h2>
		<div class="form-s-row">
			出发日期：
			<input type="text" name="tsdate" class="datepicker w80" />
			&nbsp;&nbsp;&nbsp;&nbsp; 出发：
			<select name="tscid" class="w80">
				<% for(int k : cmap.keySet()){ %>
				<option value="<%=k %>"><%=cmap.get(k) %></option>
				<% } %>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp; 到达：
			<select name="tfcid" class="w80">
				<% for(int k : cmap.keySet()){ %>
				<option value="<%=k %>"><%=cmap.get(k) %></option>
				<% } %>
			</select>
			&nbsp;&nbsp;&nbsp;&nbsp; 车次：
			<select name="tkind" class="w100">
				<option value="5">所有</option>
				<option value="0">K(快车)</option>
				<option value="1">T(特快)</option>
				<option value="2">D(动车)</option>
				<option value="3">G(高铁)</option>
				<option value="4">L(临时)</option>
			</select>
			<input type="text" name="tcode" class="w80" />
		</div>

		<div class="form-s-btn">
			<button class="btn btn-primary" type="submit">
				查 询
			</button>
		</div>
	</form>

	

</div>
<!--/row-->

<!-- 加载底部  -->
<%@ include file="inc/footer.jsp"%>
<script type="text/javascript">
	$('.datepicker').datepicker({
		format : 'yyyy-mm-dd',
		language : 'zh-CN'
	});
</script>