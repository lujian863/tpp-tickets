<%@ page language="java" import="java.util.*,cn.njau.tpp.dbo.*" pageEncoding="UTF-8"%>
<jsp:useBean id="cmb" scope="page" class="cn.njau.tpp.bean.CommonBean" />

<%
	List<Order> orders = cmb.getMyOrder(Integer.parseInt(request.getSession().getAttribute("uid").toString()));
	Map<Integer,String> cmap = cmb.getCities();
	Map<Integer, String> kind = new HashMap<Integer, String>();
	kind.put(0, "K(快车)");
	kind.put(1, "T(特快)");
	kind.put(2, "D(动车)");
	kind.put(3, "G(高铁)");
	kind.put(4, "L(临时)");
%>
<!-- 加载头部  -->
<%@ include file="inc/header.jsp"%>
<div class="container-fluid mt50">

	<div class="order-list">
		<table class="table table-hover">
			<thead>
				<tr>
					<th>
						姓名
					</th>
					<th>
						身份证号
					</th>
					<th>
						车次
					</th>
					<th>
						出发城市
					</th>
					<th>
						出发时间
					</th>
					<th>
						到达城市
					</th>
					<th>
						到达时间
					</th>
					<th>
						价格
					</th>
					<th>
						操作
					</th>
				</tr>
			</thead>
			<%
				for(Order o : orders){
				Train t = new Train(o.getTid());
			%>
			<form method="post" action="order.del">
				<input type="hidden" name="oid" value="<%=o.getOid() %>" />
				<tr>
					<td>
						<%=o.getMname() %>
					</td>
					<td>
						<%=o.getMcode() %>
					</td>
					<td>
						<%=kind.get(t.getTkind()) %><%=t.getTcode() %>
					</td>
					<td>
						<%=cmap.get(t.getTscid()) %>
					</td>
					<td>
						<%=t.getTstime() %>
					</td>
					<td>
						<%=cmap.get(t.getTfcid()) %>
					</td>
					<td>
						<%=t.getTftime() %>
					</td>
					<td>
						<%=t.getTprice() %>元
					</td>
					<td>
						<input type="submit" class="btn btn-primary btn-small" value="退订" />
					</td>
				</tr>
			</form>
			<%} %>

		</table>
	</div>

</div>
<!--/row-->
<!-- 加载底部  -->
<%@ include file="inc/footer.jsp"%>
<script>
	$(document).ready(function() {
		$('form').submit(function() {
			return confirm('确定退订？');
		});
	});
</script>