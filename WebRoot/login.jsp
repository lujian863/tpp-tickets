<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>火车票预订系统</title>
		<meta http-equiv="pragma" content="no-cache">
		<meta http-equiv="cache-control" content="no-cache">
		<meta http-equiv="expires" content="0">
		<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
		<meta http-equiv="description" content="This is my page">

		<link rel="stylesheet" href="css/base.css" media="screen" />
		<link rel="stylesheet" href="css/bootstrap.min.css" media="screen" />
		<link rel="stylesheet" href="css/bootstrap-responsive.min.css" media="screen" />
		<link rel="stylesheet" href="css/common.css" media="screen" />
		<script src="js/jquery-1.8.0.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/common.js"></script>
	</head>

	<body>
		<div class="navbar navbar-fixed-top">
			<div class="navbar-inner">
				<div class="container-fluid">
					<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a class="brand" href="#">火车票预订系统</a>
					<div class="nav-collapse collapse">
						<ul class="nav navbar-text pull-right">
							<li>
								<a class="navbar-link" href="#regModal" data-toggle="modal">注册</a>
							</li>
						</ul>
						<ul class="nav">
							<li class="active">
								<a href="#">首页</a>
							</li>
							<li>
								<a href="#aboutModal" data-toggle="modal">关于</a>
							</li>
						</ul>
					</div>
					<!--/.nav-collapse -->
				</div>
			</div>
		</div>

<div class="container-fluid mt30">
	<div class="container-fluid mt50 mb100">
		<form class="form-signin" method="post" action="login.do">
			<h2 class="form-signin-heading" align="center">
				会员登录
			</h2>
			<input type="text" name="umail" class="input-block-level" placeholder="Email address">
			<input type="password" name="upass" class="input-block-level" placeholder="Password">
			还没有账号？&nbsp;&nbsp;
			<a href="#regModal" data-toggle="modal">我要注册</a>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			<button class="btn btn-large btn-primary" type="submit">
				登录
			</button>
		</form>


	</div>
	<!--/row-->
</div>
<!--/row-->

<!-- 加载底部  -->
<%@ include file="inc/footer.jsp"%>
