<%@ page language="java" import="java.util.*,cn.njau.tpp.dbo.Man" pageEncoding="UTF-8"%>
<jsp:useBean id="cmb" scope="page" class="cn.njau.tpp.bean.CommonBean" />

<!-- 加载头部  -->
<%@ include file="inc/header.jsp"%>
<%
	List<Man> men = cmb.getMyMen((Integer) request.getSession().getAttribute("uid"));
%>
<div class="container-fluid mt30">
	<div class="search-list">
		<h1 align="center">
			常用名单管理
		</h1>
		<p class="text-center">
			<a href="#manModal" data-toggle="modal" class="btn btn-primary btn-small">添加常用名单</a>
		</p>
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>
						姓名
					</th>
					<th>
						身份证号
					</th>
					<th>
						是否默认
					</th>
					<th>
						操作
					</th>
				</tr>
			</thead>
			<%
				for (Man m : men) {
			%>
			<tr>
				<td>
					<%=m.getMname()%>
				</td>
				<td>
					<%=m.getMcode()%>
				</td>
				<td>
					<%
						if (m.getIsdefault() == 0) {
					%>
					是
					<%
						} else {
					%>
					否
					<%
						}
					%>
				</td>
				<td>
					<%
						if (m.getIsdefault() == 1) {
					%>
					<a href="man.do?action=set&id=<%=m.getId()%>" class="btn btn-mini btn-primary">设置为默认</a>
					<%
						} else {
					%>
					<a href="man.do?action=uset&id=<%=m.getId()%>" class="btn btn-mini btn-primary">取消默认</a>
					<%
						}
					%>
					<a href="#" class="btn btn-mini btn-primary">修改</a>
					<a href="man.do?action=del&id=<%=m.getId()%>" class="btn btn-mini btn-primary">删除</a>
				</td>
			</tr>
			<%
				}
			%>
		</table>
	</div>
</div>
<!--/row-->

<div id="manModal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
			&times;
		</button>
		<h3>
			添加常用名单
		</h3>
	</div>
	<div class="modal-body">
		<form class="form-horizontal" method="post" action="man.do">
			<input type="hidden" name="action" value="add">
			<div class="control-group">
				<label class="control-label" for="inputUname">
					姓名：
				</label>
				<div class="controls">
					<input type="text" name="m_name" placeholder="姓名">
				</div>
			</div>
			<div class="control-group">
				<label class="control-label" for="inputEmail">
					身份证号码：
				</label>
				<div class="controls">
					<input type="text" name="m_code" placeholder="身份证号">
				</div>
			</div>
			<div class="control-group">
				<div class="controls">
					<button type="submit" class="btn btn-primary">
						添加
					</button>
					<button class="btn" data-dismiss="modal">
						取消
					</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- 加载底部  -->
<%@ include file="inc/footer.jsp"%>