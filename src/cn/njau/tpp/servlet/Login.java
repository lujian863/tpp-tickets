package cn.njau.tpp.servlet;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import cn.njau.tpp.conf.Conf;
import cn.njau.tpp.dbo.User;
import cn.njau.tpp.util.Info;
import cn.njau.tpp.util.StrUtils;

public class Login extends HttpServlet implements Servlet {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding(Conf.ENCODING);
		response.setContentType(Conf.CONTENTTYPE);
		
		String umail = request.getParameter("umail").trim();
		String upass = request.getParameter("upass").trim();
		upass = StrUtils.md5(upass);
		HttpSession session = request.getSession();// 获取Session
		
		// 验证提交的信息是否合法
		boolean isValid = Info.isLoginInfoValid(umail, upass);
		if(isValid){
			System.out.println("登录信息合法！");
			// 合法，验证用户是否存在，存在:设置User对象，不存在，
			User user = new User(umail, upass);
			if(user.isExist()){
				System.out.println("查询到此用户！");
				//注册Session，跳转到首页
				session.setAttribute("uid",user.getUid());
				session.setAttribute("uname",user.getUname());
				session.setAttribute("umail",user.getUmail());
				session.setAttribute("ukind",user.getUkind());
				session.setAttribute("ukindx",user.getUkindx());
				
				if(user.getUkind() == 0){
					response.sendRedirect("index.jsp");
				}else if(user.getUkind() == 1){
					response.sendRedirect("admin/index.jsp");
				}
				
			}else{
				// 不存在此用户
				System.out.println("登录失败2！");
				response.sendRedirect("login.jsp");
			}
		}else{
			// 不合法
			System.out.println("登录信息不合法！");
			System.out.println("登录失败1！");
			response.sendRedirect("login.jsp");
		}
	}
}
