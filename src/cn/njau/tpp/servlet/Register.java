package cn.njau.tpp.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.njau.tpp.conf.Conf;
import cn.njau.tpp.util.DBUtils;
import cn.njau.tpp.util.Info;
import cn.njau.tpp.util.StrUtils;

public class Register extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {
		try {
			doPost(request, response);
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding(Conf.ENCODING);
		response.setContentType(Conf.CONTENTTYPE);

		String uname = request.getParameter("uname").trim();
		String umail = request.getParameter("umail").trim();
		String upass = request.getParameter("upass").trim();
		String upass2 = request.getParameter("upass2").trim();

		// 验证提交的信息是否合法
		boolean isValid = Info.isRegInfoValid(uname, umail, upass, upass2);

		if (!isValid) {
			// 不合法，跳转到错误页面
			// 运用forward方法只能重定向到同一个Web应用程序中的一个资源;而sendRedirect方法可以让你重定向到任何URL
			System.out.println("注册失败！");
			response.sendRedirect("error.jsp");
		} else {

			// 验证注册的 Email 是否已经存在
			// if()
			
			Map<String, String> data = new HashMap<String, String>();
			data.put("uname", uname);
			data.put("umail", umail);
			data.put("upass", StrUtils.md5(upass));// 对密码进行加密处理

			// 合法，写进数据库，跳转到登录页面
			if (DBUtils.insert("tb_user", data)) {
				response.sendRedirect("login.jsp");
				System.out.println("写入成功");
			} else {
				response.sendRedirect("error.jsp");
				System.out.println("写入失败");
			}
		}
	}

}
