package cn.njau.tpp.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.njau.tpp.conf.Conf;
import cn.njau.tpp.util.DBUtils;

public class ManActions extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public static final String ADD = "add";
	public static final String DEL = "del";
	public static final String SET = "set";
	public static final String UST = "uset";
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		doPost(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		
		request.setCharacterEncoding(Conf.ENCODING);
		response.setContentType(Conf.CONTENTTYPE);
		
		String action = request.getParameter("action").trim();
		int uid = (Integer)request.getSession().getAttribute("uid");
		
		// 添加人员
		if(action.equals(ADD)){
			String m_name = request.getParameter("m_name");
			String m_code = request.getParameter("m_code");
			String sql = "insert into tb_man (uid, m_name, m_code) values ";
			sql += "("+ uid +", '"+ m_name +"','"+ m_code +"' )";
			int ds = DBUtils.update(sql);
			if(ds > 0){
				response.sendRedirect("main.jsp");
			}else{
				response.sendRedirect("error.jsp");
			}
		}
		
		// 删除人员
		if(action.equals(DEL)){
			String id = request.getParameter("id");
			if(DBUtils.delete("tb_man", id)){
				response.sendRedirect("main.jsp");
			}else{
				response.sendRedirect("error.jsp");
			}
		}
		
		// 设为默认，取消其他默认
		if(action.equals(SET)){
			String id = request.getParameter("id");
			String sql = "update tb_man set m_default = 0 where id = " + id;
			int tpp = DBUtils.update(sql);
			if(tpp > 0){
				response.sendRedirect("main.jsp");
			}else{
				response.sendRedirect("error.jsp");
			}
		}
		
		// 取消默认
		if(action.equals(UST)){
			String id = request.getParameter("id");
			String sql = "update tb_man set m_default = 1 where id = " + id;
			int tpp = DBUtils.update(sql);
			if(tpp > 0){
				response.sendRedirect("main.jsp");
			}else{
				response.sendRedirect("error.jsp");
			}
		}
	}
	
}
