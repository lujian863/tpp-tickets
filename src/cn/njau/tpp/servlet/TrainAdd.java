package cn.njau.tpp.servlet;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.njau.tpp.conf.Conf;
import cn.njau.tpp.util.DBUtils;

public class TrainAdd extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding(Conf.ENCODING);
		response.setContentType(Conf.CONTENTTYPE);
		
		String tscid = request.getParameter("tscid");
		String tfcid = request.getParameter("tfcid");
		String tstime = request.getParameter("tstime");
		String tftime = request.getParameter("tftime");
		String tkind = request.getParameter("tkind");
		String tcode = request.getParameter("tcode");
		String tprice = request.getParameter("tprice");
		String tnum = request.getParameter("tnum");
		
		Map<String, String> data = new HashMap<String, String>();
		data.put("tscid", tscid);
		data.put("tfcid", tfcid);
		data.put("tstime", tstime);
		data.put("tftime", tftime);
		data.put("tkind", tkind);
		data.put("tcode", tcode);
		data.put("tprice", tprice);
		int tid = DBUtils.insertWithReId("tb_train", data);
		
		Map<String, String> data2 = new HashMap<String, String>();
		data2.put("tid", Integer.toString(tid));
		data2.put("num", tnum);
		
		if(DBUtils.insert("tb_ticket", data2)){
			System.out.println("添加成功");
			response.sendRedirect("trains.jsp");
		}else{
			System.out.println("添加失败");
		}
	}
}
