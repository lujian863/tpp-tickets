package cn.njau.tpp.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.njau.tpp.conf.Conf;
import cn.njau.tpp.dbo.Order;

public class Book extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding(Conf.ENCODING);
		response.setContentType(Conf.CONTENTTYPE);

		// 获取订单信息
		int tid = Integer.parseInt(request.getParameter("tid"));
		int uid = (Integer) request.getSession().getAttribute("uid");
		
		int max = Conf.MAX_TICKETS;
		List<Order> olist = new ArrayList<Order>();
		int js = 0;
		int i = 1;
		while(js <= max){
			String mname = request.getParameter("tname-"+i);
			String mcode = request.getParameter("mcode-"+i);
			i++;
			if( mname != null && mcode != null){
				js++;
				olist.add(new Order(uid, tid, mname, mcode));
			}
		}
		
		// 对 MySQL 进行锁操作
		
		// 添加订单，以下可进行事务封装
//		boolean pro = true;
//		int i = 0;
//		for(Order o : olist){
//			i++;
//			System.out.println("shit!!!===========" + i);
//			try {
//				if(o.save() == 0){
//					pro = false;
//				}
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			} catch (Exception e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//		}
//		
//		if(pro){
//			response.sendRedirect("order.jsp");
//		}else{
//			response.sendRedirect("error.jsp");
//		}
		response.sendRedirect("order.jsp");
	}
}
