package cn.njau.tpp.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.njau.tpp.bean.CommonBean;
import cn.njau.tpp.conf.Conf;
import cn.njau.tpp.dbo.Train;

public class Search extends HttpServlet implements Servlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		request.setCharacterEncoding(Conf.ENCODING);
		response.setContentType(Conf.CONTENTTYPE);
		
		String tsdate = request.getParameter("tsdate").trim();
		int tscid = Integer.parseInt(request.getParameter("tscid"));
		int tfcid = Integer.parseInt(request.getParameter("tfcid"));
		int tkind = Integer.parseInt(request.getParameter("tkind"));
		String tcode = request.getParameter("tcode").trim();
		
		Train st = new Train();
		st.setTcode(tcode);
		st.setTfcid(tfcid);
		st.setTscid(tscid);
		st.setTkind(tkind);
		st.setTsdate(tsdate);
		List<Train> tmp = new ArrayList<Train>();
		try {
			List<Train> tlist = new CommonBean().getTrains();
			for(Train t : tlist){
				if(t.isLike(st)){
					tmp.add(t);
				}
			}
			request.setAttribute("tlist",tmp);
			request.getRequestDispatcher("train.jsp").forward(request,response);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
