package cn.njau.tpp.bean;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import cn.njau.tpp.dbo.Man;
import cn.njau.tpp.dbo.Order;
import cn.njau.tpp.dbo.User;
import cn.njau.tpp.util.DBUtils;

public class AdminBean {

	private List<User> users = null;
	private List<Order> orders = null;
	private List<Man> men = null;
	
	public List<Man> getMen() throws SQLException {
		if (men != null) {
			return men;
		}
		men = new ArrayList<Man>();
		String criteria = "order by id";
		ResultSet rs = DBUtils.query("tb_man", criteria);
		if (rs != null) {
			while (rs.next()) {
				Man m = new Man();
				m.setMname(rs.getString("m_name"));

				men.add(m);
			}
		} else {
			men = null;
		}
		return men;
	}

	public List<User> getUsers() throws SQLException {
		if (users != null) {
			return users;
		}
		users = new ArrayList<User>();
		String criteria = "order by uid";
		ResultSet rs = DBUtils.query("tb_user", criteria);
		while (rs.next()) {
			users.add(new User(rs.getInt("uid"), rs.getString("uname"), rs.getString("umail"), rs.getInt("ukind")));
		}
		return users;
	}

	public List<Order> getAllOrders() {
		if (orders != null) {
			return orders;
		}
		orders = new ArrayList<Order>();
		String criteria = "order by oid";
		ResultSet rs = DBUtils.query("tb_order", criteria);
		try {
			while (rs.next()) {
				
				int oid = rs.getInt("oid");
				int uid = rs.getInt("uid");
				int tid = rs.getInt("tid");
				int mid = rs.getInt("mid");
				
				Man m = new Man(mid);
				String mname = m.getMname();
				String mcode = m.getMcode();
				Order order = new Order(oid, uid, tid, mid, mname, mcode);
				
				orders.add(order);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orders;
	}
}
