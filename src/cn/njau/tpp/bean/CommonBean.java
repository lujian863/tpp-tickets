package cn.njau.tpp.bean;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.njau.tpp.dbo.Man;
import cn.njau.tpp.dbo.Order;
import cn.njau.tpp.dbo.Train;
import cn.njau.tpp.util.DBUtils;

public class CommonBean {

	private Map<Integer, String> cities = null;
	private List<Train> trains = null;
	private List<Order> orders = null;
	
	public CommonBean() {

	}
	
	public Map<Integer, Integer> getTickets() throws SQLException{
		Map<Integer, Integer> tk = new HashMap<Integer, Integer>();
		String criteria = "order by tid";
		ResultSet rs = DBUtils.query("tb_ticket",criteria);
		while(rs.next()){
			tk.put(rs.getInt("tid"), rs.getInt("num"));
		}
		return tk;
	}
	
	public int getTicket(int tid) throws SQLException{
		int t = 0;
		String criteria = "where tid = " + tid;
		ResultSet rs = DBUtils.query("tb_ticket",criteria);
		if(rs.next()){
			t = rs.getInt("num");
		}
		return t;
	}
		
	public List<Man> getMyMen(int uid) throws SQLException{
		List<Man> men = new ArrayList<Man>();
		String criteria = "where uid = " + uid;
		ResultSet rs = DBUtils.query("tb_man",criteria);
		while(rs.next()){
			Man m = new Man();
			m.setId(rs.getInt("id"));
			m.setMname(rs.getString("m_name"));
			m.setMcode(rs.getString("m_code"));
			m.setIsdefault(rs.getInt("m_default"));
			m.print();
			men.add(m);
		}
		return men;
	}
	

	public Map<Integer, String> getCities() throws SQLException {
		if (cities != null) {
			return cities;
		}
		cities = new HashMap<Integer, String>();
		String criteria = "order by cid";
		ResultSet rs = DBUtils.query("tb_city", criteria);
		while (rs.next()) {
			cities.put(rs.getInt("cid"), rs.getString("cname"));
		}
		return cities;
	}
	
	public List<Train> getTrains() throws SQLException {
		trains = new ArrayList<Train>();
		String criteria = "order by tid";
		ResultSet rs = DBUtils.query("tb_train", criteria);
		if (rs != null) {
			while (rs.next()) {
				Train t = new Train();
				t.setTscid(rs.getInt("tscid"));
				t.setTfcid(rs.getInt("tfcid"));
				t.setTcode(rs.getString("tcode"));
				t.setTstime(rs.getString("tstime"));
				t.setTftime(rs.getString("tftime"));
				t.setTkind(rs.getInt("tkind"));
				t.setTid(rs.getInt("tid"));
				t.setTprice(rs.getInt("tprice"));
				trains.add(t);
			}
		}else{
			trains = null;
		}
		return trains;
	}
	
	public List<Order> getMyOrder(int uid){
		
		orders = new ArrayList<Order>();
		String criteria = "where uid = " + uid;
		ResultSet rs = DBUtils.query("tb_order", criteria);
		try {
			while(rs.next()){
				int oid = rs.getInt("oid");
				int tid = rs.getInt("tid");
				int mid = rs.getInt("mid");
				Man m = new Man(mid);
				String mname = m.getMname();
				String mcode = m.getMcode();
				Order order = new Order(oid,uid,tid,mid,mname,mcode);
				orders.add(order);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return orders;
	}

}
