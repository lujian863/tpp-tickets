package cn.njau.tpp.conf;

public class Conf {
	
	// 配置
	public static final String ENCODING = "UTF-8";// 编码
	public final static String CONTENTTYPE="text/html;charset=utf-8";
	public static final String WEB_TITLE = "火车票预订系统";// 网站名称，标题
	public static final int MAX_TICKETS = 5;// 最多一次购买票数

}
