package cn.njau.tpp.util;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;




// 封装所有的数据库操作
public class DBUtils {
	
	private static DBConn db = new DBConn();
	
	// 执行删除
	public static boolean delete(String tb, int id){
		return delete(tb,"id",id);
	}
	
	public static boolean delete(String tb, String id){
		return delete(tb,"id",id);
	}
	
	public static boolean delete(String tb, String idn, String id){
		String sql = "delete from "+ tb +" where "+ idn +" = "+ id;
		if(update(sql) > 0){
			return true;
		}
		return false;
	}
	
	public static boolean delete(String tb, String idn, int id){
		String sql = "delete from "+ tb +" where "+ idn +" = "+ id;
		if(update(sql) > 0){
			return true;
		}
		return false;
	}
	
	// 执行添加，并返回ID
	public static int insertWithReId(String tbName, Map<String,String> data){
		String sql = mapToSQL(tbName,data);
		Statement stmt = db.getStatement();
		int id = 0;
		try {
			stmt.executeQuery("set names utf8");
			int rs = stmt.executeUpdate(sql,Statement.RETURN_GENERATED_KEYS);
			ResultSet rs2 = stmt.executeQuery("SELECT LAST_INSERT_ID()");  
			if(rs > 0 && rs2.next()){  
			      id = rs2.getInt(1);
			}  
		} catch (Exception e) {
			System.out.println("异常出现了。。。。~~");
		}
		return id;
	}
	
	
	// 执行添加
	public static boolean insert(String tbName, Map<String,String> data){
		
		String sql = mapToSQL(tbName,data);
		int rs = update(sql);
		if(rs == 0){
			return false;
		}
		return true;
	}
	
	public static ResultSet query(String tbName, String criteria){
		String sql = "select * from " + tbName + " " + criteria;
		System.out.println("SQL : " + sql);
		return query(sql);
	}
	
	// 将 Map 转化成 SQL 语句
	public static String mapToSQL(String tbName,Map<String,String> data){
		String sql = "insert into "+ tbName + " (";
		String key = "";
		String val = "";
		for(String k : data.keySet()){
			key += k + ",";
			val += "'" + data.get(k) + "',";
		}
		key = key.substring(0,key.length() - 1);
		val = val.substring(0, val.length()- 1);
		sql += key + ") values (" +val + ") ";
		return sql;
	}
	
	// 执行查询
	public static ResultSet query(String sql) {
		Statement stmt = db.getStatement();
		ResultSet rs = null;
		try {
			stmt.executeQuery("set names utf8");
			rs = stmt.executeQuery(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			rs = null;
		}
		return rs;
	}

	// 执行更新
	public static int update(String sql) {
		Statement stmt = db.getStatement();
		int rs = 0;
		try {
			stmt.executeQuery("set names utf8");
			rs = stmt.executeUpdate(sql);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("错误：SQL--" + sql);
		}
		return rs;
	}
	
	
}
