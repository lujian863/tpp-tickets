package cn.njau.tpp.util;

public class Info {

	
	// 判断注册信息有效性
	public static boolean isRegInfoValid(String uname, String umail, String upass, String upass2) {
		if(!upass.equals(upass2)){
			return false;
		}
		if(uname == null || umail == null || upass == null){
			return false;
		}
		if(uname == "" || umail == "" || upass == ""){
			return false;
		}
		return true;
	}
	
	public static boolean isLoginInfoValid(String umail, String upass){
		if(umail == null || umail == "" || upass == null || upass == ""){
			return false;
		}
		return true;
	}

}
