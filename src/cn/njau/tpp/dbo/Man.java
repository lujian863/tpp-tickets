package cn.njau.tpp.dbo;

import java.sql.ResultSet;
import java.sql.SQLException;

import cn.njau.tpp.util.DBUtils;

public class Man {

	private int id = 0;
	private String mname = null;// 姓名
	private String mcode = null;// 身份证号
	private int isdefault = 1;//是否默认  0-默认 1-否
	
	public Man(){
		
	}
	
	public Man(int id) throws SQLException{
		ResultSet rs = DBUtils.query("tb_man", "where id = " + id );
		if(rs.next()){
			this.id = id;
			this.mname = rs.getString("m_name");
			this.mcode = rs.getString("m_code");
			this.isdefault = rs.getInt("m_default");
		}
	}
	
	public Man(String mname, String mcode, int isdefault){
		this.mname = mname;
		this.mcode = mcode;
		this.isdefault = isdefault;
	}

	public void print(){
		System.out.println("*************** One Man *******************");
		System.out.println("ID :" + id);
		System.out.println("姓名：" + mname);
		System.out.println("身份证号：" + mcode);
		System.out.println("是否默认：" + isdefault);
	}
	
	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getMname() {
		return mname;
	}

	public void setMcode(String mcode) {
		this.mcode = mcode;
	}

	public String getMcode() {
		return mcode;
	}

	public void setIsdefault(int isdefault) {
		this.isdefault = isdefault;
	}

	public int getIsdefault() {
		return isdefault;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}
	
}
