package cn.njau.tpp.dbo;

import java.util.HashMap;
import java.util.Map;

import cn.njau.tpp.util.DBUtils;
import cn.njau.tpp.util.ManUtils;

public class Order {

	private int oid = 0;
	private int uid = 0;
	private int tid = 0;
	private int mid = 0;
	
	private String mname;
	private String mcode;

	public Order() {
	}

	public Order(int uid, int tid, int mid) {
		this.uid = uid;
		this.tid = tid;
		this.mid = mid;
	}

	public Order(int oid, int uid, int tid, int mid) {
		this.oid = oid;
		this.uid = uid;
		this.tid = tid;
		this.mid = mid;
	}
	
	public Order(int uid, int tid, String mname, String mcode){
		this.uid = uid;
		this.tid = tid;
		this.mname = mname;
		this.mcode = mcode;
	}
	
	public Order(int oid, int uid, int tid, int mid, String mname, String mcode){
		this.oid = oid;
		this.uid = uid;
		this.tid = tid;
		this.mid = mid;
		this.mname = mname;
		this.mcode = mcode;
	}

	// 使用连接池重写
	public int save() throws Exception {
		
		if(ManUtils.getMid(mcode) == 0){
			Map<String, String> data = new HashMap<String, String>();
			data.put("m_name", mname);
			data.put("m_code", mcode);
			
			this.mid = DBUtils.insertWithReId("tb_man", data);
		}
		
		if(mid == 0){
			this.mid = ManUtils.getMid(mcode);
		}
		
		String sql = "insert into tb_order (uid, tid, mid) values (" + uid + "," + tid + ",'" + mid + "')";
		//System.out.println("sql  = " + sql);
		return DBUtils.update(sql);
	}

	public void setUid(int uid) {
		this.uid = uid;
	}

	public int getUid() {
		return uid;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public int getTid() {
		return tid;
	}

	public void setMid(int mid) {
		this.mid = mid;
	}

	public int getMid() {
		return mid;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}

	public int getOid() {
		return oid;
	}

	public void setMname(String mname) {
		this.mname = mname;
	}

	public String getMname() {
		return mname;
	}

	public void setMcode(String mcode) {
		this.mcode = mcode;
	}

	public String getMcode() {
		return mcode;
	}

}