package cn.njau.tpp.dbo;

import java.sql.ResultSet;
import java.sql.SQLException;

import cn.njau.tpp.util.DBUtils;

public class User {
	
	private boolean isExist = false;
	private int uid;
	private String uname;
	private String umail;
	private int ukind;
	private String ukindx;

	public User(int uid, String uname, String umail, int ukind) {
		this.uid = uid;
		this.uname = uname;
		this.umail = umail;
		this.setUkind(ukind);
	}

	public User(String umail, String upass) {
		String sql = "where umail = '" + umail + "' and upass = '" + upass + "'";
		ResultSet rs = DBUtils.query("tb_user", sql);
		try {
			if (rs.next()) {
				this.uid = rs.getInt("uid");
				this.uname = rs.getString("uname");
				this.setUkind(rs.getInt("ukind"));
				this.isExist = true;
			} else {
				this.uid = 0;
				this.uname = "未登录";
				this.umail = umail;
				this.setUkind(0);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getUname() {
		return uname;
	}

	public void setUmail(String umail) {
		this.umail = umail;
	}

	public String getUmail() {
		return umail;
	}

	private void setUkind(int kind) {
		this.ukind = kind;
		this.setUkindx(kind);
	}

	public int getUkind() {
		return ukind;
	}

	private void setUkindx(int kind) {
		String tmp = "";
		switch (kind) {
		case 0:
			tmp = "会员";
			break;
		case 1:
			tmp = "管理员";
			break;
		default:
			tmp = "异常";
			break;
		}
		this.ukindx = tmp;
	}

	public String getUkindx() {
		return ukindx;
	}

	public int getUid() {
		return uid;
	}

	public boolean isExist() {
		return isExist;
	}

}
