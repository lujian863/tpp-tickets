package cn.njau.tpp.dbo;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import cn.njau.tpp.util.DBUtils;

public class Train {
	
	private int tid;//
	private int tscid;// 出发城市ID
	private int tfcid;// 到达城市ID
	private String tstime;// 出发时间
	private String tftime;// 到达时间
	private String tsdate;// 出发日期，这个字段便于检索
	private int tkind;// 类别 K,T,D,G,L
	private String tcode;// 编号
	private int tprice;

	public Train() {
		// donothing
	}

	public Train(int tid) throws SQLException {
		String criteria = "where tid = " + tid;
		ResultSet rs = DBUtils.query("tb_train", criteria);
		if (rs != null) {
			while (rs.next()) {
				this.setTid(tid);
				this.setTscid(rs.getInt("tscid"));
				this.setTfcid(rs.getInt("tfcid"));
				this.setTstime(rs.getString("tstime"));
				this.setTftime(rs.getString("tftime"));
				this.setTkind(rs.getInt("tkind"));
				this.setTcode(rs.getString("tcode"));
				this.setTprice(rs.getInt("tprice"));
			}
		}
	}

	public Train(int id, int scid, int fcid, String stime, String ftime, int kind, String code, int price, int num) {
		this.setTid(id);
		this.setTscid(scid);
		this.setTfcid(fcid);
		this.setTstime(stime);
		this.setTftime(ftime);
		this.setTkind(kind);
		this.setTcode(code);
		this.setTprice(price);
	}

	// 这个方法不对，会报错。。。
	public boolean save() {
		Map<String, String> data = new HashMap<String, String>();
		data.put("tscid", Integer.toString(tscid));
		data.put("tfcid", Integer.toString(tfcid));
		data.put("tstime", tstime);
		data.put("tftime", tftime);
		data.put("tkind", Integer.toString(tkind));
		data.put("tcode", tcode);
		data.put("tprice", Integer.toString(tprice));
		return DBUtils.insert("tb_train", data);
	}

	// 是否相似（提供查询）
	public boolean isLike(Train t) {
		if (t.tcode != null && t.tcode != "" && !t.tcode.equals(this.tcode)) {
			return false;
		}
		if (!t.tsdate.equals(this.tsdate)) {
			return false;
		}
		if(t.tkind == 5){
			if (t.tfcid != this.tfcid || t.tscid != this.tscid) {
				return false;
			}
		}else{
			if (t.tfcid != this.tfcid || t.tscid != this.tscid || t.tkind != this.tkind) {
				return false;
			}
		}
		
		return true;
	}

	public void setTscid(int tscid) {
		this.tscid = tscid;
	}

	public int getTscid() {
		return tscid;
	}

	public void setTfcid(int tfcid) {
		this.tfcid = tfcid;
	}

	public int getTfcid() {
		return tfcid;
	}

	public void setTstime(String tstime) {
		this.tstime = tstime;
		this.tsdate = tstime.substring(0, 10);
	}

	public String getTstime() {
		return tstime;
	}

	public void setTftime(String tftime) {
		this.tftime = tftime;
	}

	public String getTftime() {
		return tftime;
	}

	public void setTkind(int tkind) {
		this.tkind = tkind;
	}

	public int getTkind() {
		return tkind;
	}

	public void setTcode(String tcode) {
		this.tcode = tcode;
	}

	public String getTcode() {
		return tcode;
	}

	public String getTsdate() {
		return tsdate;
	}

	public void setTid(int tid) {
		this.tid = tid;
	}

	public int getTid() {
		return tid;
	}

	public void setTprice(int tprice) {
		this.tprice = tprice;
	}

	public int getTprice() {
		return tprice;
	}

	public void setTsdate(String tsdate) {
		this.tsdate = tsdate;
	}

}
